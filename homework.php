<?php

    class TelegraphText
    {
        private $title;
        private $text;
        private $author;
        private $published;
        private $slug;

        public function __construct($author, $slug)
        {
            $this->setAuthor($author);
            $this->setSlug($slug);
            $this->setPublished(date("D - d - F - Y"));
        }

        public function __get($name)
        {
            if ($name == 'author') {
                return $this->getAuthor();
            } elseif ($name == 'published') {
                return $this->getPublished();
            } elseif ($name == 'slug') {
                return $this->getSlug();
            } elseif ($name == 'text') {
                return $this->loadText();
            }
        }

        public function __set($name, $value)
        {
            if ($name == 'author') {
                return $this->setAuthor($value);
            } elseif ($name == 'published') {
                return $this->setPublished($value);
            } elseif ($name == 'slug') {
                return $this->setSlug($value);
            } elseif ($name == 'text') {
                return $this->storeText();
            }
        }

        public function getAuthor()
        {
            return $this->author;
        }

        public function setAuthor($author)
        {
            if (strlen($author) <= 120) {
                $this->author = $author;
            }
        }

        public function getPublished()
        {
            return $this->published;
        }

        public function setPublished($published)
        {
            if ($published >= date("D - d - F - Y")) {
                $this->published = $published;
            }
        }

        public function getSlug()
        {
            return $this->slug;
        }

        public function setSlug($slug)
        {
            if (preg_match('/^[a-zA-Z0-9_.-]*$/', $slug)) {
                $this->slug = $slug;
            }
        }

        private function storeText()
        {
            $textStorage = array(
                'title' => $this->title,
                'author' => $this->author,
                'text' => $this->text,
                'published' => $this->published
            );

            file_put_contents($this->slug, serialize($textStorage));

        }

        private function loadText()
        {
            if (file_exists($this->slug)) {

                $tempArray = unserialize(file_get_contents($this->slug));

                $this->title = $tempArray['title'];
                $this->author = $tempArray['author'];
                $this->text = $tempArray['text'];
                $this->published = $tempArray['published'];

                return $this->text;

            }

            return false;

        }

        public function editText($title, $text)
        {

            $this->title = $title;
            $this->text = $text;

        }
    }

$telegraphText = new TelegraphText('Kevin', 'telegraph-text.txt');
$telegraphText->editText('My First Telegraph Title', 'This is my first Telegraph text.');
$telegraphText->__set('text', 'This is my text');
$telegraphText->__get('text');